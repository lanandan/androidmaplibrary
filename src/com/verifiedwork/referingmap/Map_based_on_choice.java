package com.verifiedwork.referingmap;

import com.verifiedwork.androidmaplibrary.Geoloc;
import com.verifiedwork.androidmaplibrary.MainActivity;
import com.verifiedwork.androidmaplibrary.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Map_based_on_choice extends Activity implements OnClickListener 
{
	Button submit,next;
	RadioGroup rg;
	RadioButton selected;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
	      super.onCreate(savedInstanceState);
	      setContentView(R.layout.activity_simple_map_based_on_choice);      	      
	      submit=(Button)findViewById(R.id.submit);
	      submit.setOnClickListener(this);
	      
	      next=(Button)findViewById(R.id.Next);
	      next.setOnClickListener(this);
	      
	      rg=(RadioGroup)findViewById(R.id.radioGroup);	      
	}
	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.submit)
		{
			int selectedId = rg.getCheckedRadioButtonId();
			selected = (RadioButton) findViewById(selectedId);
			String selectedoperation=selected.getText()+"";
			Intent in = new Intent(this,MainActivity.class);
			in.putExtra("selected operation",selectedoperation);
			startActivity(in);			
		}		
		
		if(v.getId()==R.id.Next)
		{
			Intent in = new Intent(this,Geoloc.class);			
			startActivity(in);			
		}
	}
}
