package com.verifiedwork.referingmap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.verifiedwork.androidmaplibrary.Geofencing_Sample;
import com.verifiedwork.androidmaplibrary.MainActivity;
import com.verifiedwork.androidmaplibrary.Map_with_marker_choice;
import com.verifiedwork.androidmaplibrary.R;


public class Map_based_on_marker extends Activity implements OnClickListener 
{
	Button submit,next;
	RadioGroup marker_group;
	RadioButton marker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
	      super.onCreate(savedInstanceState);
	      setContentView(R.layout.activity_marker_based_on_choice);      	      
	      submit=(Button)findViewById(R.id.submit_marker);
	      submit.setOnClickListener(this);
	      next=(Button)findViewById(R.id.Next_marker);
	      next.setOnClickListener(this);
	      marker_group=(RadioGroup)findViewById(R.id.radioGroup);	      
	}
	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.submit_marker)
		{
			int selectedId = marker_group.getCheckedRadioButtonId();
			marker = (RadioButton) findViewById(selectedId);
			String selectedoperation=marker.getTag()+"";
			Toast.makeText(this,selectedoperation+"",Toast.LENGTH_LONG).show();
			Intent in = new Intent(this,Map_with_marker_choice.class);
			in.putExtra("selected operation",selectedoperation);
			startActivity(in);	
		}			
		if(v.getId()==R.id.Next_marker)
		{
			Intent in = new Intent(this,Geofencing_Sample.class);			
			startActivity(in);				
		}
	}
}
