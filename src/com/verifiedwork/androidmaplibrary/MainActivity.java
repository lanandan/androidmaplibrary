package com.verifiedwork.androidmaplibrary;

import java.util.List;
import java.util.Locale;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.widget.Toast;

public class MainActivity extends Activity implements LocationListener
{
   private GoogleMap googleMap;
   Location location;
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
	   super.onCreate(savedInstanceState);
      
      setContentView(R.layout.activity_main);
      String selectedoperation=getIntent().getExtras().getString("selected operation");
      	try {
         if (googleMap == null) 
         {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
         }
         googleMap.setMyLocationEnabled(true);
         if(selectedoperation.equals("Normal"))
         {
        	 googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);		 
         }
         if(selectedoperation.equals("Satellite"))
         {
        	 googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);		 
         }
         if(selectedoperation.equals("Hybrid"))
         {
        	 googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);		 
         }
         if(selectedoperation.equals("Terrian"))
         {
        	 googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);		 
         }
        
         LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
         Criteria criteria = new Criteria();
         String bestProvider = locationManager.getBestProvider(criteria, true);
         Location location = locationManager.getLastKnownLocation(bestProvider);
         final LatLng Verifiedwork = new LatLng(location.getLatitude(),location.getLongitude());
         googleMap.addMarker(new MarkerOptions().position(Verifiedwork).title("GR. Infotech"));
         googleMap.moveCamera(CameraUpdateFactory.newLatLng(Verifiedwork));
         googleMap.animateCamera(CameraUpdateFactory.zoomTo(50));                 
         
        /*if (location != null) {
             onLocationChanged(location);
         }
         locationManager.requestLocationUpdates(bestProvider, 20000, 0, this);*/
      }
      catch (Exception e) {
         e.printStackTrace();
      }
   }

@Override
public void onLocationChanged(Location location) {
	// TODO Auto-generated method stub
	double latitude = location.getLatitude();
    double longitude = location.getLongitude();
    LatLng latLng = new LatLng(latitude, longitude);
    googleMap.addMarker(new MarkerOptions().position(latLng));
    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));	
}

@Override
public void onStatusChanged(String provider, int status, Bundle extras) {
	// TODO Auto-generated method stub
	
}

@Override
public void onProviderEnabled(String provider) {
	// TODO Auto-generated method stub
	
}

@Override
public void onProviderDisabled(String provider) {
	// TODO Auto-generated method stub
	
}
}

