package com.verifiedwork.androidmaplibrary;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.EditText;

public class Geoloc extends Activity
{
		GoogleMap googleMap; 
		EditText edt;
	   @Override
	   protected void onCreate(Bundle savedInstanceState) 
	   {
		  super.onCreate(savedInstanceState);	      
	      setContentView(R.layout.activity_main);
	      edt=(EditText)findViewById(R.id.Reports);
	      googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
          googleMap.setMyLocationEnabled(true);   
          drawCircle(new LatLng(38.907098,-77.036455));
          drawMarker(new LatLng(38.907098,-77.036455),1);          
          drawMarker(new LatLng(38.907983,-77.034094),2);
          drawMarker(new LatLng(38.908100,-77.039652),3);
          drawMarker(new LatLng(38.905779,-77.039073),4);
          drawMarker(new LatLng(38.909252,-77.039287),5);
          drawMarker(new LatLng(38.906914,-77.040725),6);
          drawMarker(new LatLng(38.904960,-77.044930),7);          
          googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(38.9047,-77.0164)));      
          edt.setText("James Jacob"+"In time 10.00 A.M"+"\n"+"Jacob Ethan"+"In time 11.10 A.M"+"\n"+"Daniel Anthony"+"In time 11.10 A.M"+"\n"+"William Alexander"+"In time 02.10 P.M"+"\n"+"Michael Ryan"+"In time 04.00 P.M");          
	   }
	   
	   
	   private void drawCircle(LatLng point)
	   {
	        CircleOptions circleOptions = new CircleOptions();
	        circleOptions.center(point);
	        circleOptions.radius(2000);
	        circleOptions.strokeColor(Color.BLACK);
	        circleOptions.fillColor(0x30ff0000);
	        circleOptions.strokeWidth(2);
	        googleMap.addCircle(circleOptions);
	    }
	   
	   private void drawMarker(LatLng point, int i){
	        MarkerOptions markerOptions = new MarkerOptions();
	        markerOptions.position(point);
	        
	        if(i==1)
	        {
	        	googleMap.addMarker(new MarkerOptions()
	        	.position(point)
	            .title("You")
	            .snippet("Your Location")
	            .icon(BitmapDescriptorFactory.fromResource(R.drawable.rman)));	
	        
	        }
	        if(i==2)
	        {
	        	googleMap.addMarker(new MarkerOptions()  
	        	.position(point)
	            .title("James Jacob")
	            .snippet("Available Taxi")
	            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_mrkr)));	
	        
	        }
	        if(i==3)
	        {
	        	googleMap.addMarker(new MarkerOptions()
	        	.position(point)	        	
	            .title("Jacob Ethan")
	            .snippet("Available Taxi")
	            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_mrkr)));	
	        
	        }
	        if(i==4)
	        {
	        	googleMap.addMarker(new MarkerOptions()
	        	.position(point)
	            .title("Daniel Anthony")
	            .snippet("Available Taxi")
	            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_mrkr)));	
	        
	        }
	        if(i==5)
	        {
	        	googleMap.addMarker(new MarkerOptions()
	        	.position(point)
	            .title("William Alexander")
	            .snippet("Available Taxi")
	            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_mrkr)));	
	        
	        }
	        if(i==6)
	        {
	        	googleMap.addMarker(new MarkerOptions()
	        	.position(point)
	            .title("Alexander Jacob")
	            .snippet("Available Taxi")
	            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_mrkr)));	
	        
	        }
	        if(i==7)
	        {
	        	googleMap.addMarker(new MarkerOptions()
	        	.position(point)
	            .title("Michael Ryan")
	            .snippet("Available Taxi")
	            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_mrkr)));	
	        
	        }	        
	    }
	   

}
