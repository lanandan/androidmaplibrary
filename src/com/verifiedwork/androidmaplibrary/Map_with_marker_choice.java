package com.verifiedwork.androidmaplibrary;

import java.util.List;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.app.Activity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressLint("NewApi")
public class Map_with_marker_choice extends Activity implements LocationListener {
GoogleMap map;
@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_main);
LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
map.setMapType(GoogleMap.MAP_TYPE_HYBRID);	

}

@Override
public void onLocationChanged(Location location) 
{
map.clear();
MarkerOptions mp = new MarkerOptions();
//mp.position(new LatLng(location.getLatitude(), location.getLongitude()));
mp.position(new LatLng(32.787279600000000000,-116.828693199999980000));

mp.title("GR. Infotech");
String selectedoperation=getIntent().getExtras().getString("selected operation");

if(selectedoperation.equals("blue"))
{
	mp.icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_blank));
			 
}
if(selectedoperation.equals("red"))
{
	mp.icon(BitmapDescriptorFactory.fromResource(R.drawable.redblank));		 
}
if(selectedoperation.equals("green"))
{
	mp.icon(BitmapDescriptorFactory.fromResource(R.drawable.greenblank));		 
}
if(selectedoperation.equals("yellow"))
{
	mp.icon(BitmapDescriptorFactory.fromResource(R.drawable.yellowblank));			 
}
map.addMarker(mp);

//map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 32));       
map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(32.787279600000000000,-116.828693199999980000), 32));


}

@Override
public void onProviderDisabled(String provider) {
// TODO Auto-generated method stub

}

@Override
public void onProviderEnabled(String provider) {
// TODO Auto-generated method stub

}

@Override
public void onStatusChanged(String provider, int status, Bundle extras) {
// TODO Auto-generated method stub

}

}



